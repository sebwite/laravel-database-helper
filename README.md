# Laravel Database Helper #

Package die het mogelijk maakt om de laatste SQL query met syntax highlight te tonen

## Versies ##
### Versie 1.0 ###
* Initial commit - incl. de mogelijkheid om een formatted query te printen.

## Installatie ##


Voeg de volgende repo toe aan Composer.json


```
#!php

{
     "type": "vcs",
      "url": "https://SilvanLaroo@bitbucket.org/SilvanLaroo/laravel-database-helper.git"
}
```

Require package in composer.json


```
#!php
"sebwite/db-helper" : "dev-master"

```
Laat composer de package installeren door composer update te draaien.

Voeg de ServiceProvider toe aan app.php:

```
#!php
'Sebwite\DBHelper\DBHelperServiceProvider'
```