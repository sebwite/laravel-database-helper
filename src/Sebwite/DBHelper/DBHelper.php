<?php namespace Sebwite\DBHelper;

use DB;
use SqlFormatter;

class DBHelper {

    /**
     * Returns last query formatted
     *
     * @param $return
     * @return string
     */
    public static function lastQuery($return = false)
    {
        if($return)
            return DBHelper::getQuery();

        echo SqlFormatter::format(DBHelper::getQuery());
        dd();
    }

    /**
    * Returns given query formatted
    *
    */
    public static function query($queryId) {
        $queries = DB::getQueryLog();

        if( isset($queries[$queryId]) ) {
            echo SqlFormatter::format(DBHelper::getQuery($queries[$queryId]));
            dd();
        }
    }

    /**
     * Returns the last performed database query
     *
     * @return mixed
     */
    private static function getQuery($sql = false){
        
        if( ! $sql) {
            $queries = DB::getQueryLog();
            $sql = end($queries);
        }

        if( ! empty($sql['bindings']))
        {
            $pdo = DB::getPdo();
            foreach($sql['bindings'] as $binding)
            {
                $sql['query'] =
                    preg_replace('/\?/', $pdo->quote($binding),
                        $sql['query'], 1);
            }
        }

        return $sql['query'];
    }

}