<?php namespace Sebwite\DBHelper;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class DBHelperServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    /**
     *
     */
    public function boot()
    {
        $this->package('sebwite/dbhelper');

        AliasLoader::getInstance()->alias('DBHelper', 'Sebwite\DBHelper\DBHelper');
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('dbhelper');
	}

}
